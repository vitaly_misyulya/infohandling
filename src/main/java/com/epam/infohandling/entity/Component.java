package com.epam.infohandling.entity;

import java.util.Collection;
import java.util.List;


public interface Component {

    ComponentType getType();

    void addComponent(Component c);

    void addComponents(Collection<? extends  Component> c);

    Component getChild(int index);

    List<Component> getChildren();

    void remove(Component c);

    String getContent();

    /**
     * Collects all component's children whose type equals typeToCollect to collectedComponents list
     *
     * @param typeToCollect       type of component children to collect
     * @param component           Component whose children will be collected
     * @param collectedComponents list of collected Components
     */
    static void collectComponentsWithType(Component component, ComponentType typeToCollect, List<Component> collectedComponents) {
        if (component.getType() != typeToCollect) {
            if (component.getType().isComposite()) {
                for (Component child : component.getChildren()) {
                    collectComponentsWithType(child, typeToCollect, collectedComponents);
                }
            }
        } else {
            collectedComponents.add(component);
        }
    }

    /**
     * Calculates count of component's children  whose type equals childType
     *
     * @param childType type of children to count
     * @param component Component whose children will be counted
     * @return
     */
    static int calcCountOfChildrenWithType(Component component, ComponentType childType) {
        int count = 0;
        if (component.getType().isComposite()) {
            for (Component child : component.getChildren()) {
                if (child.getType() == childType) {
                    count++;
                } else if (child.getType().isComposite()) {
                    count += calcCountOfChildrenWithType(child, childType);
                }
            }
        }
        return count;
    }
}
