package com.epam.infohandling.entity;

public enum ComponentType {
    TEXT(true), PARAGRAPH(true), LISTING(false), SENTENCE(true), WORD(false), PUNCTUATION_MARK(false),
    SPACE(false), END_OF_LINE(false), NUMBER(false), HEADER_NUMBER(false);

    private final boolean isComposite;

    ComponentType(boolean isComp) {
        isComposite = isComp;
    }

    public boolean isComposite() {
        return isComposite;
    }
}
