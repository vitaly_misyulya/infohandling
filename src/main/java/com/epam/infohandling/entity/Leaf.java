package com.epam.infohandling.entity;

import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.List;


public class Leaf implements Component {

    private static final Logger LOG = Logger.getLogger(Leaf.class);

    private ComponentType type;
    private String content;

    public Leaf(ComponentType type, String content) {
        setType(type);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        if (!type.isComposite()) {
            this.type = type;
        } else {
            LOG.error(type + "is composite");
        }
    }

    @Override
    public void addComponent(Component c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addComponents(Collection<? extends Component> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Component getChild(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Component> getChildren() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(Component c) {
        throw new UnsupportedOperationException();
    }
}
