package com.epam.infohandling.entity;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class TextComposite implements Component {

    private static final Logger LOG = Logger.getLogger(TextComposite.class);

    private List<Component> children = new ArrayList<>();
    private ComponentType type;

    public TextComposite(ComponentType type) {
        setType(type);
    }

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        if (type.isComposite()) {
            this.type = type;
        } else {
            LOG.error(type + " isn't composite");
        }
    }

    @Override
    public void addComponent(Component c) {
        children.add(c);
    }

    @Override
    public void addComponents(Collection<? extends Component> c) {
        children.addAll(c);
    }

    @Override
    public Component getChild(int index) {
        return children.get(index);
    }

    @Override
    public List<Component> getChildren() {
        return Collections.unmodifiableList(children);
    }

    @Override
    public void remove(Component c) {
        children.remove(c);
    }

    @Override
    public String getContent() {
        String str = "";
        String child;
        ComponentType prevType = ComponentType.PARAGRAPH;
        for (Component c : children) {
            child = c.getContent();
            switch (c.getType()) {
                case PARAGRAPH:
                    child += "\n";
                    break;
                case SPACE:
                    if (prevType == ComponentType.END_OF_LINE) {
                        child = child.trim();
                    }
                    break;
            }
            str += child;
            prevType = c.getType();
        }
        return str;
    }
}
