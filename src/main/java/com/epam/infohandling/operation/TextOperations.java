package com.epam.infohandling.operation;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.ComponentType;
import com.epam.infohandling.entity.TextComposite;
import com.epam.infohandling.util.ResourceManager;
import com.epam.infohandling.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class TextOperations {

    private static final String SENTENCE_FORMAT = ResourceManager.INSTANCE.getString("str-format.sentence");

    public static String sortTextSentencesByWordCount(TextComposite text) {
        List<Component> sentences = new ArrayList<>();
        Component.collectComponentsWithType(text, ComponentType.SENTENCE, sentences);

        Map<String, Integer> map = sentences.stream()
                .collect(Collectors.toMap(c -> c.getContent().replaceAll("\\s", " "),   /*replace end of line by space inside sentence */
                        c -> Component.calcCountOfChildrenWithType(c, ComponentType.WORD)));

        List<Map.Entry<String, Integer>> sortedList = Util.sortMapByValueToList(map);

        StringBuilder result = new StringBuilder();
        sortedList.stream()
                .filter(e -> e.getValue() > 0)
                .forEach(e -> result.append(String.format(SENTENCE_FORMAT, e.getValue(), e.getKey())));
        return result.toString();
    }

    public static String sortTextWordsAlphabetically(TextComposite text, boolean deleteDuplicates) {
        List<Component> words = new ArrayList<>();
        StringBuilder result = new StringBuilder();
        Component.collectComponentsWithType(text, ComponentType.WORD, words);

        if (words.size() > 0) {
            Predicate<String> distinctValuePredicate = (deleteDuplicates)
                    ? Util.distinctByKey(String::toLowerCase)
                    : s -> true;

            List<String> sortedWords = words.stream()
                    .map(Component::getContent)
                    .sorted(String::compareToIgnoreCase)
                    .filter(distinctValuePredicate)
                    .collect(Collectors.toList());

            result.append(sortedWords.get(0));
            if (sortedWords.size() > 1) {
                for (int i = 1; i < sortedWords.size(); i++) {
                    result = result.append(sortedWords.get(i).toLowerCase().charAt(0) == sortedWords.get(i - 1).toLowerCase().charAt(0)
                            ? " "
                            : "\n").append(sortedWords.get(i));
                }
            }
        } else {
            result.append("No words to sort");
        }
        return result.toString();
    }
}
