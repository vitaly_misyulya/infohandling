package com.epam.infohandling.parser;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.ComponentType;
import com.epam.infohandling.entity.Leaf;
import com.epam.infohandling.entity.TextComposite;
import com.epam.infohandling.exception.LogicException;
import com.epam.infohandling.util.ResourceManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {

    private static final Logger LOG = Logger.getLogger(TextParser.class);
    private static final String HEADER_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.header");
    private static final String PARAGRAPH_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.paragraph");
    private static final String TEXT_FORMAT = ResourceManager.INSTANCE.getString("str-format.text");

    private static final String SENTENCE_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.sentence");
    private static final String HEADER_FORMAT = ResourceManager.INSTANCE.getString("str-format.header");

    private static final String GAP_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.gap");
    private static final String EOL_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.eol");
    private static final String PUNCT_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.punct");
    private static final String HEADER_NUMBER_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.header-number");
    private static final String WORD_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.word");
    private static final String INTEGER_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.integer");
    private static final String DECIMAL_REG_EX = ResourceManager.INSTANCE.getString("reg-ex.decimal");
    private static final String NUMBER_REG_EX = DECIMAL_REG_EX + "|" + INTEGER_REG_EX;

    public static List<Component> parseText(String text) {
        text = String.format(TEXT_FORMAT,text);    /* otherwise last paragraph won't match PARAGRAPH_REG_EX */
        List<Component> textComponents = new ArrayList<>();
        Pattern paragraphPattern = Pattern.compile(HEADER_REG_EX + "|" + PARAGRAPH_REG_EX, Pattern.DOTALL);
        Matcher paragraphMatcher = paragraphPattern.matcher(text);
        int prevEndInd = 0;         /* index of last character of previous matching group */
        String listing;
        while (paragraphMatcher.find()) {
            if (paragraphMatcher.start() > prevEndInd + 1) {
                /* Everything that doesn't match paragraphPattern is Listing */
                listing = text.substring(prevEndInd, paragraphMatcher.start()).trim();
                textComponents.add(new Leaf(ComponentType.LISTING, listing + "\n"));
            }
            prevEndInd = paragraphMatcher.end();
            TextComposite paragraph = new TextComposite(ComponentType.PARAGRAPH);
            List<Component> paragraphComponents = parseParagraph(paragraphMatcher.group());
            paragraph.addComponents(paragraphComponents);
            textComponents.add(paragraph);
        }
        return textComponents;
    }

    public static List<Component> parseParagraph(String paragraph) {
        List<Component> paragraphComponents = new ArrayList<>();
        boolean isHeader = false;
        if (Pattern.matches(HEADER_REG_EX, paragraph)) {
            paragraph = String.format(HEADER_FORMAT, paragraph.trim());  /* Otherwise last sentence in header won't match SENTENCE_REG_EX */
            isHeader = true;
        }
        Pattern sentencePattern = Pattern.compile(SENTENCE_REG_EX, Pattern.DOTALL);
        Matcher sentenceMatcher = sentencePattern.matcher(paragraph);
        String sentenceStr;
        while (sentenceMatcher.find()) {
            sentenceStr = sentenceMatcher.group().trim();
            if (sentenceMatcher.end() == paragraph.length() && isHeader) {
                sentenceStr = sentenceStr.substring(0, sentenceStr.length() - 1); /* Deletes . in last sentence in header */
            }
            TextComposite sentence = new TextComposite(ComponentType.SENTENCE);
            List<Component> sentenceComponents = parseSentence(sentenceStr);
            sentence.addComponents(sentenceComponents);
            paragraphComponents.add(sentence);
        }
        return paragraphComponents;
    }

    public static List<Component> parseSentence(String sentence) {
        List<Component> sentenceComponents = new ArrayList<>();
        sentence = sentence.replaceAll(GAP_REG_EX, " ");
        boolean isFirstSubSentence = true;
        /* Split into sub sentences to save EOL characters inside sentence */
        String[] subSentences = sentence.split(EOL_REG_EX);
        for (String sub : subSentences) {
            if (!isFirstSubSentence) {
                sentenceComponents.add(new Leaf(ComponentType.END_OF_LINE, "\n"));
            } else {
                isFirstSubSentence = false;
            }
            String[] expressions = sub.split(GAP_REG_EX);
            for (String expr : expressions) {
                sentenceComponents.add(new Leaf(ComponentType.SPACE, " "));
                sentenceComponents.addAll(parseExpression(expr));
            }
        }
        return sentenceComponents;
    }

    /**
     * Parses expression which can be simple (word, number, punctuation mark)
     * or complex - consisting of few simple expressions
     *
     * @param expr expression to parse
     * @return list of sentence components
     */
    private static List<Component> parseExpression(String expr) {
        List<Component> sentenceComponents = new ArrayList<>();
        Pattern wordPattern = Pattern.compile(NUMBER_REG_EX + "|" + WORD_REG_EX + "|" + PUNCT_REG_EX);
        Matcher wordMatcher = wordPattern.matcher(expr);
        String simpleExpr;
        ComponentType type = ComponentType.WORD;
        /* Since numbers match WORD_REG_EX, check for NUMBER_REG_EX should forestall check for WORD_REG_EX*/
        if (expr.matches(NUMBER_REG_EX)) {
            sentenceComponents.add(new Leaf(ComponentType.NUMBER, expr));
        } else if (expr.matches(WORD_REG_EX)) {
            sentenceComponents.add(new Leaf(ComponentType.WORD, expr));
        } else if (expr.matches(PUNCT_REG_EX)) {
            sentenceComponents.add(new Leaf(ComponentType.PUNCTUATION_MARK, expr));
        } else if (expr.matches(HEADER_NUMBER_REG_EX)) {
            sentenceComponents.add(new Leaf(ComponentType.HEADER_NUMBER, expr));
        } else {
            while (wordMatcher.find()) {
                simpleExpr = wordMatcher.group();
                try {
                    type = determineSimpleExprType(simpleExpr);
                } catch (LogicException ex) {
                    LOG.error(ex.getMessage());
                }
                Leaf leaf = new Leaf(type, simpleExpr);
                sentenceComponents.add(leaf);
            }
        }
        return sentenceComponents;
    }

    private static ComponentType determineSimpleExprType(String simpleExpression) throws LogicException {
        ComponentType type;
        if (simpleExpression.matches(NUMBER_REG_EX)) {
            type = ComponentType.NUMBER;
        } else if (simpleExpression.matches(WORD_REG_EX)) {
            type = ComponentType.WORD;
        } else if (simpleExpression.matches(PUNCT_REG_EX)) {
            type = ComponentType.PUNCTUATION_MARK;
        } else {
            throw new LogicException(String.format("Simple expression:\"%s\" doesn't match any RegEx", simpleExpression));
        }
        return type;
    }
}
