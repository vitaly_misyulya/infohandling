package com.epam.infohandling.report;

import org.apache.log4j.Logger;

import java.io.*;


public class ReportText {

    private static final Logger LOG = Logger.getLogger(ReportText.class);

    private StringBuilder report = new StringBuilder();

    public void appendString(String str) {
        report.append(str);
    }

    public void clearReport() {
        report = new StringBuilder();
    }

    public boolean writeReportToFile(String filePath) {
        boolean isSuccessfulWrite = true;
        File file = new File(filePath);
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            pw.print(report);
        } catch (IOException e) {
            LOG.error("Can't write report to file: " + file.getAbsolutePath());
            isSuccessfulWrite = false;
        }
        return isSuccessfulWrite;
    }
}
