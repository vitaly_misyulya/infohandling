package com.epam.infohandling.run;

import com.epam.infohandling.entity.Component;
import com.epam.infohandling.entity.ComponentType;
import com.epam.infohandling.entity.TextComposite;
import com.epam.infohandling.operation.TextOperations;
import com.epam.infohandling.parser.TextParser;
import com.epam.infohandling.report.ReportText;
import com.epam.infohandling.util.ResourceManager;
import com.epam.infohandling.util.Util;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class Runner {

    private static final String REPORT_FILE_PATH = ResourceManager.INSTANCE.getString("report.file-path");
    private static final String REPORT_FILE_EXT = ResourceManager.INSTANCE.getString("report.file-extension");
    private static final String DATE_TIME_FORMAT = ResourceManager.INSTANCE.getString("report.date-time-format");
    private static final String TEXT_FILE_PATH = ResourceManager.INSTANCE.getString("text-file-path");

    static {
        new DOMConfigurator().doConfigure("config/log4j.xml", LogManager.getLoggerRepository());
    }

    private static final Logger LOG = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        ReportText reportText = new ReportText();
        String text = "";
        try {
            text = Util.readTextFromFile(TEXT_FILE_PATH);
        } catch (FileNotFoundException e) {
            LOG.error("File " + TEXT_FILE_PATH + " not found.");
            return;
        }
        TextComposite textComp = new TextComposite(ComponentType.TEXT);
        List<Component> textComponents = TextParser.parseText(text);
        textComp.addComponents(textComponents);

        reportText.appendString(String.format("Restored text:%n%s", textComp.getContent()));

        String firstOperationResult = TextOperations.sortTextSentencesByWordCount(textComp);
        reportText.appendString(String.format("%nText sentences sorted by word count:%n%s", firstOperationResult));

        String secondOperationResult = TextOperations.sortTextWordsAlphabetically(textComp, true);
        reportText.appendString(String.format("%nText words sorted alphabetically:%n%s", secondOperationResult));

        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        reportText.writeReportToFile(REPORT_FILE_PATH + ldt.format(dateTimeFormatter) + REPORT_FILE_EXT);
    }
}
