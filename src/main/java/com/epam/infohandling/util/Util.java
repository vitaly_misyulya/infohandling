package com.epam.infohandling.util;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class Util {

    private static final Logger LOG = Logger.getLogger(Util.class);

    /**
     * Reads text from file to String
     *
     * @param path      path to text file
     * @return          String containing whole text
     * @throws FileNotFoundException
     */
    public static String readTextFromFile(String path) throws FileNotFoundException {
        return new Scanner(new File(path)).useDelimiter("\\A").next();
    }

    /**
     * Reads text from file to List<String>
     *
     * @param path      path to text file
     * @return          List<String> containing strings(lines) from text file
     */
    public static List<String> readStringsFromFile(String path) {
        List<String> strings = new LinkedList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader(path))) {
            String tmp;
            while ((tmp = bf.readLine()) != null) {
                strings.add(tmp);
            }
        } catch (IOException e) {
            LOG.error("Can't read file " + path);
        }
        return strings;
    }

    /**
     * Returns Predicate that can be used to filter out only distinct(unique) objects from stream of T objects
     * where uniqueness defined by result of applying keyExtractor function to T object.
     *
     * @param keyExtractor      Function applied to object T to define uniqueness of object
     * @param <T>
     * @return
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /**
     * Sorts map by value and return sorted List<Map.Entry<K,V>>
     *
     * @param map   map to sort
     * @param <K>   type of key
     * @param <V>   type of value
     * @return      List<Map.Entry<K,V>> of map entries sorted by value
     */
    public static <K, V extends Comparable<? super V>> List<Map.Entry<K, V>> sortMapByValueToList(Map<K, V> map) {
        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toList());
    }

    /**
     * Sorts map by value and return sorted LinkedHashMap<K,V>>
     *
     * @param map   map to sort
     * @param <K>   type of key
     * @param <V>   type of value
     * @return      sorted LinkedHashMap<K,V>>
     */
    public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortMapByValue(Map<K, V> map) {
        LinkedHashMap<K, V> sortedMap = new LinkedHashMap<>();
        map.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .forEach(e -> sortedMap.put(e.getKey(), e.getValue()));
        return sortedMap;
    }
}
